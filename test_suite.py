import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS2D_PATH
if os.getenv('ELLIPSYS2D_PATH') == None:
    raise RuntimeError('ELLIPSYS2D_PATH environment variable not set!')


class DU00_W_212_re15e6_tran_drela(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_tran_drela',
                         'inputfile':'input.dat',
#                        'flowfield': 'flowfieldMPI',
                         'inputs':[{'mstep':[100, 100, 100]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'trstart': 10},
                                   {'trlevel': 3},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'test_restart': True,
                         'debug': True,
                         'nprocs': 4}


class DU00_W_212_re15e6_turb(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_turb',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100, 100, 100]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'test_restart': True,
                         'debug': True,
                         'nprocs': 4}


class DU00_W_212_re15e6_turb_serial(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_turb',
                         'flowfield': 'flowfield',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100, 100, 100]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'test_restart': True,
                         'debug': True,
                         'nprocs': 1}


class DU00_W_212_re15e6_turb_uns(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_turb_uns',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100, 100, 100]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'test_restart': True,
                         'debug': True,
                         'nprocs': 4}


class DU00_W_212_re15e6_turb_uns_pitching(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_turb_solidbody_mov',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[100, 200, 200]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'test_restart': True,
                         'debug': True,
                         'nprocs': 4}


"""
class DU00_W_212_re15e6_turbPY(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'directory': 'ellipsys2d_airfoil_testcases/DU00-W-212/re15e6_turb',
                         'inputfile':'input.dat',
                         'exec_mode': 'python',
                         'flowfield': 'flowfieldMPI.py',
                         'inputs':[{'mstep':[100, 100, 100]},
                                   {'uinlet':1.0}, {'vinlet':.0},
                                   {'ufarfield':1.0}, {'vfarfield':.0},
                                   {'project':'grid'}],
                         'variables':['fx_ave', 'fy_ave'],
                         'nprocs': 4}
"""

if __name__ == '__main__':

    unittest.main()
