# EllipSys2D Test Suite Runner

This repository contains the test cases for EllipSys2D.

## Prerequisites

To use the automated test case runner, you need to have Python 2.7.x installed.
An easy way to installing this is to use Miniconda:

    # choose a location to install miniconda
    $ export CONDA_ENV_PATH=/path/to/miniconda
    $ export PATH=$CONDA_ENV_PATH/bin:$PATH

    $ wget --quiet \
    https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh && \
    bash Miniconda-latest-Linux-x86_64.sh -b -p $CONDA_ENV_PATH && \
    rm Miniconda-latest-Linux-x86_64.sh && \
    chmod -R a+rx $CONDA_ENV_PATH

    $ conda update --quiet --yes conda \
      && conda create -y -n py27 python=2.7 \
      && /bin/bash -c "source activate py27 \
      && conda install pip numpy scipy nose hdf5"


## Installation

To run the test cases through the regression test tool for EllipSys,
you need to install the `ellipsyswrapper` module, which
is a simple Python wrapper for EllipSys2D and EllipSys3D, that also contains
a script for executing test cases. Alternatively, you can run the testcases
manually, and you can skip this section.

First step is to clone and install the `ellipsyswrapper` git repository

    $ source activate py27
    $ pip install git+https://gitlab.windenergy.dtu.dk/EllipSys/ellipsyswrapper.git@master

You also need to specify where the Python wrapper can find the executables for EllipSys.
This is done by setting an environment variable:

    $ export ELLIPSYS2D_PATH=/path/to/ellipsys2d/


### Downloading testcases

As opposed to the EllipSys3D testcases, all test cases are contained in this repository, 
so no download is needed. Simply clone this repository:

    $ git clone https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys2d_testcases.git

## Running the testsuite

With the above steps completed you can now run the test suite, simply by invoking the command:

    $ python test_suite.py

which should finalize with an `OK`.

## Running the testcases manually

If you don't want to run the testcases through Python, you can run EllipSys2D
manually:

    $ cd ellipsys2d_airfoil_testcases/DU00-W-212
    $ mkdir run_re15e6_turb
    $ cp re15e6_turb/input.dat run_re15e6_turb
    $ cd run_re15e6_turb
    $ ln -s ../grid/grid.*2D .
    $ mpirun -np <nprocs> /path/to/flowfieldMPI
    
where `<nprocs>` is the number of processors to use. For these tests you can
run with a maximum of 32 cores since that is the number of blocks in the grid.
Note that these tests do not run EllipSys2D to convergence, but only run 100
iterations on each grid level.
    

